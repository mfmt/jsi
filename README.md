# Jitsi Meet Simultaneous Interpretation

This repository contains the code to provide language tools for use with a Jitsi Meet installation.

This code is for you *if*:

 * You have a working Jitsi Meet installation.
 * You have the ability to host a second, separate web site.
 * You have a real human being who can conduct simultaneous interpretation for you.
 * You only have two different languages in play (e.g. English and Spanish
   speakers)
 * All participants are using modern web browsers on desktop computers or via a
   web browser on newish Android devices (it does not work via the desktop
   electron app or either of the mobile apps)

When working properly, you can run meetings that work exactly like a normal
Jitsi Meet installation, except there is a slider at the top of the page:

![!Slider for interpretation](/img/slider-screengrab.png)

When you slide the selector to "Interpreter" you only hear the interpreter.
When you slide it to "Live" you only hear everyone else. If you put it in the
middle, you hear both.

## How does it work?

The interpreter must include the word "interpret" in their display name. When
the interpreter hears language A, they interpret into language B. When the
interpreter hears language B they interpret into Language A. Multiple
interpreters can be on the call and trade on and off interpretation duty.

When the program runs, it pulls in your Jitsi Meet instance via an iframe. When
each participant changes their slider, it adjusts the local volume of all
participants based on whether or not they have the word "interpret" in their
display name.

## Installation

You must setup a separate, second web site in addition to your Jitsi Meet
installation. The web site only needs to host static HTML files. It should be
configured with with a separate domain (e.g. if your Jitsi Meet site is
accessible via https://meet.yourdomain.org/, you might setup this site as:
https://i.meet.yourdomain.org).

Once you have your web site setup, copy `index.html.sample` to `index.html` and
change the domain name in two places at the top of the file and you are ready
to go.

## Web site setup configuration

Your web server should be configured so that all requests go to index.html.

### Nginx 

Here's a sample nginx configuration file. You should pick a new  `server_name` - one that is different from your regular Jitsi Meet domain name.

```
server {
  server_name i.meet.your.domain.org; 
  root  /path/to/jsi;
  location / {
    alias /path/to/jsi/;
    try_files $uri /index.html;
  }
}
```

### Apache

If you are using apache, you can simply copy the lines below into a file named
.htaccess in the directory holding your index.html file:

```
RewriteEngine On
RewriteBase /
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.html [L]
```
